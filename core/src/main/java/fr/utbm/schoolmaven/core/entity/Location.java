/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.utbm.schoolmaven.core.entity;

import java.beans.*;
import java.io.Serializable;

public class Location implements Serializable {

    private int id;
    private String city;

    public Location() {

    }

    public Location(String city) {
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
    @Override
    public String toString() {
        return "Location{" + "id=" + id + ", city=" + city + '}';
    }
}
